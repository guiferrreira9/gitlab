# frozen_string_literal: true

module Gitlab
  module Llm
    module Chain
      module Utils
        class PromptRoles
          def self.assistant(*strs)
            # TBD
          end

          def self.system(*strs)
            # TBD
          end

          def self.user(*strs)
            # TBD
          end
        end
      end
    end
  end
end
